import graphql, {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLSchema,
  GraphQLList,
  GraphQLNonNull,
} from 'graphql'
import axios from 'axios'

const baseUrl = 'http://localhost:3000/';

const CompanyType = new GraphQLObjectType({
  name: 'Company',
  description: 'User',
  fields: () => ({
    id: { type: GraphQLInt },
    name: { type: GraphQLString },
    description: { type: GraphQLString },
    users: {
      type: new GraphQLList(UserType),
      resolve(company, args) {
        return axios
          .get(`http://localhost:3000/companies/${company.id}/users`)
          .then(({ data }) => data)
      }
    },
  })
})

const UserType = new GraphQLObjectType({
  name: 'User',
  description: 'User',
  fields: () => ({
    id: { type: GraphQLInt },
    firstName: { type: GraphQLString },
    age: { type: GraphQLInt },
    company: {
      type: CompanyType,
      resolve(user, args) {
        return axios
          .get(`http://localhost:3000/companies/${user.companyId}`)
          .then(({ data }) => data)
      }
    },
  })
})

const RootQuery = new GraphQLObjectType({
  name: 'RootQuery',
  description: 'Root query',
  fields: {
    user: {
      type: UserType,
      args: {
        id: { type: GraphQLInt },
      },
      resolve(_, args, context) {
        return axios
          .get(`http://localhost:3000/users/${args.id}`)
          .then(({ data }) => data)
      }
    },
    users: {
      type: new GraphQLList(UserType),
      resolve(_, args) {
        return axios
          .get(`http://localhost:3000/users`)
          .then(({ data }) => data)
      }
    },
    company: {
      type: CompanyType,
      args: {
        id: { type: GraphQLInt },
      },
      resolve(_, args) {
        return axios
          .get(`http://localhost:3000/companies/${args.id}`)
          .then(({ data }) => data)
      }
    },
    companies: {
      type: new GraphQLList(CompanyType),
      resolve(_, args) {
        return axios
          .get(`http://localhost:3000/companies`)
          .then(({ data }) => data)
      }
    },
  },
})

const mutation = new GraphQLObjectType({
  name: 'Mutation',
  description: 'Mutation',
  fields: {
    addUser: {
      type: UserType,
      args: {
        firstName: {
          type: new GraphQLNonNull(GraphQLString)
        },
        age: {
          type: new GraphQLNonNull(GraphQLInt)
        },
        companyId: {
          type: GraphQLInt
        },
      },
      resolve(_, { firstName, age, companyId }) {
        return axios
          .post(`${baseUrl}users`, { firstName, age, companyId })
          .then(({ data }) => data)
      },
    },
    deleteUser: {
      type: UserType,
      args: {
        id: { type: new GraphQLNonNull(GraphQLInt) }
      },
      resolve(_, args) {
        return axios
          .delete(`${baseUrl}users/${args.id}`)
          .then(({ data }) => data)
      },
    },
    editUser: {
      type: UserType,
      args: {
        id: { type: new GraphQLNonNull(GraphQLInt) },
        firstName: {
          type: GraphQLString
        },
        age: {
          type: GraphQLInt
        },
        companyId: {
          type: GraphQLInt
        },
      },
      resolve(_, args) {
        return axios
          .patch(`${baseUrl}users/${args.id}`, args)
          .then(({ data }) => data)
      }
    },
  }
})

export default new GraphQLSchema({
  query: RootQuery,
  mutation: mutation, 
})